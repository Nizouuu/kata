package soge.fr.kata.service;

import lombok.Getter;
import lombok.Setter;
import soge.fr.kata.common.Movement;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ManageAccount {

    private List<Movement> movements = new ArrayList<>();

    public void deposit(BigDecimal amount) {
      BigDecimal balance =  movements.stream().map(m-> m.getAmount()).reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
      movements.add(Movement.builder().amount(amount).date(new Date()).balance(balance.add(amount)).build());

    }

    public void withdrawal(BigDecimal amount) {
        BigDecimal balance =  movements.stream().map(m-> m.getAmount()).reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
        movements.add(Movement.builder().amount(amount.negate()).date(new Date()).balance(balance.add(amount.negate())).build());
    }

    public void printMovements() {
       SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
       printMovement("DATE | AMOUNT | BALANCE");
       movements.stream().sorted(Comparator.comparing(Movement::getDate)).forEachOrdered(m -> {
           printMovement(sdf.format(m.getDate()) + "|"+ m.getAmount() +"|"+ m.getBalance());
       });
    }

    public void printMovement(String movement) {
        System.out.println(movement);
    }



}
