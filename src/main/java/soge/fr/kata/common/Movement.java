package soge.fr.kata.common;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@AllArgsConstructor
@ToString
@Data
@Builder
public class Movement {

    private final BigDecimal amount;

    private final Date date;

    private final BigDecimal balance;

}
