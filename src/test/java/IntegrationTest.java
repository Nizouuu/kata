import org.junit.Test;
import soge.fr.kata.service.ManageAccount;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_UP;
import static org.assertj.core.api.Assertions.assertThat;


public class IntegrationTest {

    @Test
    public void should_print_operations_history() {

        //Given
        ManageAccount manageAccount = new ManageAccount();

        //When
        manageAccount.deposit(BigDecimal.valueOf(1000).setScale(2, HALF_UP));
        manageAccount.deposit(BigDecimal.valueOf(2000).setScale(2, HALF_UP));
        manageAccount.withdrawal(BigDecimal.valueOf(500).setScale(2, HALF_UP));

        //then
        assertThat(manageAccount.getMovements()).extracting("balance")
                .hasSize(3)
                .containsOnly(
                        BigDecimal.valueOf(1000).setScale(2, HALF_UP),
                        BigDecimal.valueOf(3000).setScale(2, HALF_UP),
                        BigDecimal.valueOf(2500).setScale(2, HALF_UP)
                );



    }
}
