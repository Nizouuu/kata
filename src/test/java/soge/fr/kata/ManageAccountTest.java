package soge.fr.kata;

import org.junit.Before;
import org.junit.Test;
import soge.fr.kata.common.Movement;
import soge.fr.kata.service.ManageAccount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.math.RoundingMode.HALF_UP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class ManageAccountTest {

    private ManageAccount manageAccount;

    @Before
    public void setUp() {
        manageAccount = new ManageAccount();
    }

    @Test
    public void should_deposit_and_update_balance(){

        //Given
        BigDecimal actual = BigDecimal.valueOf(1000.00).setScale(2, HALF_UP);

        //When
        manageAccount.deposit(actual);

        //Then
        assertEquals(manageAccount.getMovements().size(), 1);
        assertTrue(manageAccount.getMovements().get(0).getAmount().equals(actual));
        assertTrue(manageAccount.getMovements().get(0).getBalance().equals(actual));
    }

    @Test
    public void should_withdrawal_and_update_balance(){

        //Given
        BigDecimal actual = BigDecimal.valueOf(1000.00).setScale(2, HALF_UP);
        BigDecimal expected = actual.negate();

        //When
        manageAccount.withdrawal(actual);

        //Then
        assertEquals(manageAccount.getMovements().size(), 1);
        assertTrue(manageAccount.getMovements().get(0).getAmount().equals(expected));
        assertTrue(manageAccount.getMovements().get(0).getBalance().equals(expected));
    }

    @Test
    public void should_print_operations_history() {

        //Given
        List<Movement> movements = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 1 ,1);
        movements.add(Movement.builder().date(calendar.getTime()).amount(BigDecimal.valueOf(1000.00)).balance(BigDecimal.valueOf(1000.00)).build());
        calendar.set(2018, 1 ,3);
        movements.add(Movement.builder().date(calendar.getTime()).amount(BigDecimal.valueOf(500.00)).balance(BigDecimal.valueOf(500.00)).build());
        calendar.set(2018, 1 ,2);
        movements.add(Movement.builder().date(calendar.getTime()).amount(BigDecimal.valueOf(700.00)).balance(BigDecimal.valueOf(700.00)).build());
        ManageAccount manageAccount = spy(ManageAccount.class);
        manageAccount.setMovements(movements);

        // when
        manageAccount.printMovements();

        //Then
         verify(manageAccount, times(1)).printMovement("DATE | AMOUNT | BALANCE");
         verify(manageAccount, times(1)).printMovement("01/02/2018|1000.0|1000.0");
         verify(manageAccount, times(1)).printMovement("02/02/2018|700.0|700.0");
         verify(manageAccount, times(1)).printMovement("03/02/2018|500.0|500.0");
    }


}
